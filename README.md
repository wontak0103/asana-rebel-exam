# Asana Rebel Android Development Challenge

The challenge is not about the content (the API response), but instead to give us a better idea of your coding style.

User Story:

As a user, I want to be able to read random cat facts with a certain length. I also want to be able to share a cat fact, if I like it.

Solution Sketch:

RecyclerView displaying the data with a slider to set the maximum length of the random cat fact.

Resources:

Cat Fact API: https://catfact.ninja/#/Cat_Facts

Submission:

Please create a git repository and send a link (preferred), or send it as a compressed archive along with any comments you may have. Again: If you have any questions, don’t hesitate to ask.