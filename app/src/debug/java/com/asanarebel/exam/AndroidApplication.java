package com.asanarebel.exam;

import com.asanarebel.exam.Timber.DebugTree;

import butterknife.ButterKnife;
import timber.log.Timber;

public class AndroidApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        ButterKnife.setDebug(true);
        Timber.plant(new DebugTree());
    }
}
