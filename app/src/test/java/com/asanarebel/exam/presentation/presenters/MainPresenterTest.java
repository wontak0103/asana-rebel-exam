package com.asanarebel.exam.presentation.presenters;

import com.asanarebel.exam.domain.interactors.GetCatFactsUseCase;
import com.asanarebel.exam.presentation.ui.listeners.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;

public class MainPresenterTest {

    private MainPresenter presenter;

    @Mock private MainView.View mockView;
    @Mock private GetCatFactsUseCase mockGetCatFactsUseCase;

    @Before
    public void setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(callable -> Schedulers.trampoline());

        presenter = new MainPresenter(mockGetCatFactsUseCase);
        presenter.setView(mockView);
    }

    @Test
    public void testDestroy() throws Exception {
        presenter.destroy();

        verify(mockGetCatFactsUseCase).dispose();
    }

    @Test
    public void testGetCatFacts() throws Exception {
        presenter.getCatFacts(anyInt(), anyInt());

        verify(mockGetCatFactsUseCase).execute(
                any(DisposableObserver.class),
                any(GetCatFactsUseCase.Params.class)
        );
    }
}