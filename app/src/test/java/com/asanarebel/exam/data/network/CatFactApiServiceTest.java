package com.asanarebel.exam.data.network;

import com.asanarebel.exam.data.models.Fact;
import com.asanarebel.exam.data.models.FactPage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import io.reactivex.observers.TestObserver;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CatFactApiServiceTest {

    private MockWebServer mockWebServer;
    private CatFactApiService mockCatFactApiService;

    @Before
    public void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();

        mockCatFactApiService = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(CatFactApiService.class);
    }

    @Test
    public void testGetCatFactSuccessResponse() throws Exception {
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setBody(makeMockCatFactBodyString())
        );

        TestObserver<Fact> observer = mockCatFactApiService.getCatFact(10).test();
        observer.awaitTerminalEvent();

        observer.assertNoErrors();
        assertFact(observer.values().get(0));
    }

    @Test
    public void testGetCatFactsSuccessResponse() throws Exception {
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setBody(makeMockCatFactsBodyString())
        );

        TestObserver<FactPage> observer = mockCatFactApiService.getCatFacts(1, 10).test();
        observer.awaitTerminalEvent();

        observer.assertNoErrors();
        assertFactPage(observer.values().get(0));
    }

    @After
    public void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    private String makeMockCatFactBodyString() {
        return "{ \"fact\" : \"Test fact.\", \"length\" : 10 }";
    }

    private void assertFact(Fact fact) {
        assertEquals(fact.fact, "Test fact.");
        assertEquals(fact.length, 10);
    }

    private String makeMockCatFactsBodyString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"total\": 311,");
        builder.append("\"per_page\": \"1000\",");
        builder.append("\"current_page\": 1,");
        builder.append("\"last_page\": 311,");
        builder.append("\"next_page_url\": \"https://catfact.ninja/facts?page=2\",");
        builder.append("\"prev_page_url\": null,");
        builder.append("\"from\": 1,");
        builder.append("\"to\": 1,");
        builder.append("\"data\": ");
        builder.append("[");
        builder.append(makeMockCatFactBodyString());
        builder.append("]");
        builder.append("}");
        return builder.toString();
    }

    private void assertFactPage(FactPage page) {
        assertEquals(page.total, 311);
        assertEquals(page.perPage, "1000");
        assertEquals(page.currentPage, 1);
        assertEquals(page.lastPage, 311);
        assertEquals(page.nextPageUrl, "https://catfact.ninja/facts?page=2");
        assertEquals(page.prevPageUrl, null);
        assertEquals(page.from, 1);
        assertEquals(page.to, 1);
        assertFalse(page.facts.isEmpty());
        assertEquals(page.facts.size(), 1);
        assertFact(page.facts.get(0));
    }
}
