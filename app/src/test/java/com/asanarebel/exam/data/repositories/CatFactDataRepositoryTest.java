package com.asanarebel.exam.data.repositories;

import com.asanarebel.exam.data.network.CatFactApiService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CatFactDataRepositoryTest {

    private CatFactDataRepository repository;

    @Mock private CatFactApiService mockCatFactApiService;

    @Before
    public void setUp() throws Exception {
        setUpMockApiService();

        repository = new CatFactDataRepository(mockCatFactApiService);
    }

    private void setUpMockApiService() {
        when(mockCatFactApiService.getCatFacts(anyInt(), anyInt()))
                .thenReturn(Observable.empty());
    }

    @Test
    public void testGetFactsHappyCase() throws Exception {
        TestObserver observer = repository.getFacts(anyInt(), anyInt()).test();
        observer.awaitTerminalEvent();

        observer.assertNoErrors();

        verify(mockCatFactApiService).getCatFacts(anyInt(), anyInt());
    }
}