package com.asanarebel.exam.domain.interactors;

import com.asanarebel.exam.domain.repositories.CatFactRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetCatFactsUseCaseTest {

    private static final int FAKE_LIMIT = 1;
    private static final int FAKE_MAX_LENGTH = 1;

    private GetCatFactsUseCase usecase;

    @Mock CatFactRepository mockRepository;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(callable -> Schedulers.trampoline());
        usecase = new GetCatFactsUseCase(mockRepository);
    }

    @Test
    public void testUseCaseObservable() {
        usecase.buildUseCaseObservable(new GetCatFactsUseCase.Params(FAKE_LIMIT, FAKE_MAX_LENGTH));

        verify(mockRepository).getFacts(FAKE_LIMIT, FAKE_MAX_LENGTH);
        verifyNoMoreInteractions(mockRepository);
    }
}