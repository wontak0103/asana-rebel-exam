package com.asanarebel.exam.data.repositories;

import com.asanarebel.exam.data.converters.NetworkModelConverter;
import com.asanarebel.exam.data.network.CatFactApiService;
import com.asanarebel.exam.domain.repositories.CatFactRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CatFactDataRepository implements CatFactRepository {

    private CatFactApiService service;

    @Inject
    public CatFactDataRepository(CatFactApiService service) {
        this.service = service;
    }

    @Override
    public Observable getFacts(int limit, int maxLength) {
        return service.getCatFacts(limit, maxLength)
                .map(page -> NetworkModelConverter.convertToDomainModel(page));
    }
}
