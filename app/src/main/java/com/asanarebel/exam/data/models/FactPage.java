package com.asanarebel.exam.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FactPage {

    @SerializedName("total")
    public int total;

    @SerializedName("per_page")
    public String perPage;

    @SerializedName("current_page")
    public int currentPage;

    @SerializedName("last_page")
    public int lastPage;

    @SerializedName("next_page_url")
    public String nextPageUrl;

    @SerializedName("prev_page_url")
    public String prevPageUrl;

    @SerializedName("from")
    public int from;

    @SerializedName("to")
    public int to;

    @SerializedName("data")
    public List<Fact> facts;
}
