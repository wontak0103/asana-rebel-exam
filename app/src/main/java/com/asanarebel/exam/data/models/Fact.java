package com.asanarebel.exam.data.models;

import com.google.gson.annotations.SerializedName;

public class Fact {

    @SerializedName("fact")
    public String fact;

    @SerializedName("length")
    public int length;
}
