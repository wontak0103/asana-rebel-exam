package com.asanarebel.exam.data.converters;

import com.asanarebel.exam.data.models.Fact;
import com.asanarebel.exam.data.models.FactPage;

import java.util.ArrayList;
import java.util.List;

public class NetworkModelConverter {

    public static com.asanarebel.exam.domain.models.Fact convertToDomainModel(Fact fact) {
        com.asanarebel.exam.domain.models.Fact toReturn = new com.asanarebel.exam.domain.models.Fact();

        toReturn.fact = fact.fact;
        toReturn.length = fact.length;

        return toReturn;
    }

    public static List<com.asanarebel.exam.domain.models.Fact> convertToDomainModel(FactPage page) {
        List<com.asanarebel.exam.domain.models.Fact> toReturn = new ArrayList();

        if (page.facts == null)
            return toReturn;

        for (Fact fact : page.facts) {
            toReturn.add(convertToDomainModel(fact));
        }

        return toReturn;
    }
}
