package com.asanarebel.exam.data.network;

import com.asanarebel.exam.data.models.Fact;
import com.asanarebel.exam.data.models.FactPage;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CatFactApiService {

    @GET("/fact")
    Observable<Fact> getCatFact(
            @Query("max_length") int maxLength
    );

    @GET("/facts")
    Observable<FactPage> getCatFacts(
            @Query("limit") int limit,
            @Query("max_length") int maxLength
    );
}
