package com.asanarebel.exam.presentation.presenters;

import com.asanarebel.exam.domain.interactors.GetCatFactsUseCase;
import com.asanarebel.exam.domain.models.Fact;
import com.asanarebel.exam.presentation.ui.listeners.MainView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class MainPresenter {

    private GetCatFactsUseCase getCatFactsUseCase;

    private MainView.View view;

    @Inject
    public MainPresenter(GetCatFactsUseCase getCatFactsUseCase) {
        this.getCatFactsUseCase = getCatFactsUseCase;
    }

    public void destroy() {
        getCatFactsUseCase.dispose();
    }

    public void setView(MainView.View view) {
        this.view = view;
    }

    public void getCatFacts(int limit, int maxLength) {
        getCatFactsUseCase.execute(
                new GetCatFactsDisposableObserver(),
                new GetCatFactsUseCase.Params(limit, maxLength)
        );
    }

    private void showCatFactsInView(List<Fact> facts) {
        view.showCatFacts(facts);
    }

    private void hideViewLoading() {
        view.hideLoading();
    }

    private void showErrorMessage(String message) {
        view.showError(message);
    }

    private final class GetCatFactsDisposableObserver extends DisposableObserver<List<Fact>> {

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideViewLoading();
            showErrorMessage(e.getMessage());
        }

        @Override
        public void onNext(List<Fact> facts) {
            showCatFactsInView(facts);
        }
    }
}
