package com.asanarebel.exam.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asanarebel.exam.R;
import com.asanarebel.exam.domain.models.Fact;
import com.asanarebel.exam.presentation.ui.listeners.MainView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatFactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MainView.RecyclerViewClickListener {

    private List<Fact> items = new ArrayList();
    private MainView.View view;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.label_name)
        TextView nameLabel;

        private MainView.RecyclerViewClickListener listener;

        public ViewHolder(View v, MainView.RecyclerViewClickListener listener) {
            super(v);

            ButterKnife.bind(this, v);
            v.setOnClickListener(this);
            this.listener = listener;
        }

        public void setup(Fact item) {
            nameLabel.setText(item.fact);
        }

        @Override
        public void onClick(View v) {
            listener.onViewClick(getAdapterPosition());
        }
    }

    public CatFactsAdapter(MainView.View view) {
        this.view = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_repository, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Fact item = items.get(position);
        ((ViewHolder) holder).setup(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewClick(int index) {
        view.onFactClick(items.get(index));
    }

    public void addNewItems(@NonNull List<Fact> items) {
        // clean up old data
        if (items != null)
            this.items.clear();

        this.items.addAll(items);

        notifyDataSetChanged();
    }
}
