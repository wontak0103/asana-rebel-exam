package com.asanarebel.exam.presentation.ui.listeners;

import android.content.Context;

import com.asanarebel.exam.domain.models.Fact;

import java.util.List;

public interface MainView {

    interface View {

        Context context();

        void showLoading();

        void hideLoading();

        void showError(String message);

        void showCatFacts(List<Fact> facts);

        void onFactClick(Fact fact);
    }

    interface RecyclerViewClickListener {

        void onViewClick(int index);
    }
}
