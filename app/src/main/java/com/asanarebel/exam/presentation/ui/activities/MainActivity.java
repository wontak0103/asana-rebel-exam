package com.asanarebel.exam.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.asanarebel.exam.R;
import com.asanarebel.exam.base.BaseActivity;
import com.asanarebel.exam.di.HasComponent;
import com.asanarebel.exam.di.components.DaggerUserComponent;
import com.asanarebel.exam.di.components.UserComponent;
import com.asanarebel.exam.di.modules.UserModule;
import com.asanarebel.exam.presentation.ui.fragments.MainFragment;

public class MainActivity extends BaseActivity implements HasComponent<UserComponent> {

    private MainFragment fragment;

    private UserComponent userComponent;

    @Override
    public UserComponent getComponent() {
        return userComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeInjector();
        initializeFragment();
    }

    private void initializeInjector() {
        userComponent = DaggerUserComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .userModule(new UserModule())
                .build();
    }

    private void initializeFragment() {
        fragment = MainFragment.newInstance();
        addFragment(R.id.content_main, fragment);
    }

    public void sharingCatFact(String fact) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, fact);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
