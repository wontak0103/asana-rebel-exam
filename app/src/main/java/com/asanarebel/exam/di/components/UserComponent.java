package com.asanarebel.exam.di.components;

import com.asanarebel.exam.di.PerActivity;
import com.asanarebel.exam.di.modules.ActivityModule;
import com.asanarebel.exam.di.modules.UserModule;
import com.asanarebel.exam.presentation.ui.fragments.MainFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UserModule.class})
public interface UserComponent {

    void inject(MainFragment fragment);
}
