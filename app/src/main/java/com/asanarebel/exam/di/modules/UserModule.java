package com.asanarebel.exam.di.modules;

import com.asanarebel.exam.di.PerActivity;
import com.asanarebel.exam.domain.interactors.GetCatFactsUseCase;
import com.asanarebel.exam.domain.repositories.CatFactRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    @PerActivity
    GetCatFactsUseCase provideGetCatFactsUseCase(CatFactRepository repository) {
        return new GetCatFactsUseCase(repository);
    }
}
