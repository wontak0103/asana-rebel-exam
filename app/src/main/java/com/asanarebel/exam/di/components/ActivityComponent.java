package com.asanarebel.exam.di.components;

import android.app.Activity;

import com.asanarebel.exam.di.PerActivity;
import com.asanarebel.exam.di.modules.ActivityModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity activity();
}
