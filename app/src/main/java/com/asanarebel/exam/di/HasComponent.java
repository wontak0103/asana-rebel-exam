package com.asanarebel.exam.di;

public interface HasComponent<T> {

    T getComponent();
}
