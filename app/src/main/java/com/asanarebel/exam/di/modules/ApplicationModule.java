package com.asanarebel.exam.di.modules;

import android.app.Application;
import android.content.Context;

import com.asanarebel.exam.R;
import com.asanarebel.exam.data.network.CatFactApiService;
import com.asanarebel.exam.data.network.CatFactCallAdapterFactory;
import com.asanarebel.exam.data.repositories.CatFactDataRepository;
import com.asanarebel.exam.domain.repositories.CatFactRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        return client;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Application application, OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(application.getResources().getString(R.string.endpoint))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CatFactCallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public CatFactApiService provideCatFactApiService(Retrofit retrofit) {
        return retrofit.create(CatFactApiService.class);
    }

    @Provides
    @Singleton
    public CatFactRepository provideCatFactRepository(CatFactDataRepository repository) {
        return repository;
    }
}
