package com.asanarebel.exam.di.components;

import com.asanarebel.exam.base.BaseActivity;
import com.asanarebel.exam.di.modules.ApplicationModule;
import com.asanarebel.exam.domain.repositories.CatFactRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity activity);

    CatFactRepository catFactRepository();
}
