package com.asanarebel.exam;

import android.app.Application;

import com.asanarebel.exam.di.components.ApplicationComponent;
import com.asanarebel.exam.di.components.DaggerApplicationComponent;
import com.asanarebel.exam.di.modules.ApplicationModule;

public class BaseApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();
    }

    private void initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
