package com.asanarebel.exam.domain.interactors;

import com.asanarebel.exam.base.BaseUseCase;
import com.asanarebel.exam.domain.models.Fact;
import com.asanarebel.exam.domain.repositories.CatFactRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GetCatFactsUseCase extends BaseUseCase<List<Fact>, GetCatFactsUseCase.Params> {

    private CatFactRepository repository;

    @Inject
    public GetCatFactsUseCase(CatFactRepository repository) {
        super(Schedulers.io(), AndroidSchedulers.mainThread());

        this.repository = repository;
    }

    @Override
    protected Observable<List<Fact>> buildUseCaseObservable(Params params) {
        return repository.getFacts(params.limit, params.maxLength);
    }

    public static final class Params {

        private final int limit;
        private final int maxLength;

        public Params(int limit, int maxLength) {
            this.limit = limit;
            this.maxLength = maxLength;
        }
    }
}