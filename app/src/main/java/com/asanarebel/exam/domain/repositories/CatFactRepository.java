package com.asanarebel.exam.domain.repositories;

import io.reactivex.Observable;

public interface CatFactRepository {

    Observable getFacts(int limit, int maxLength);
}
